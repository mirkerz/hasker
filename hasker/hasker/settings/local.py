from .base import *  # noqa

SECRET_KEY = "FOO"
DEBUG = True
TEMPLATES[0]["OPTIONS"]["debug"] = DEBUG

DEBUG_TOOLBAR_CONFIG = {
    "INTERCEPT_REDIRECTS": False,
    # 'SHOW_TOOLBAR_CALLBACK': (
    #     'base.settings.base.custom_show_toolbar'),
    "HIDE_DJANGO_SQL": True,
    "TAG": "body",
    "SHOW_TEMPLATE_CONTEXT": True,
    "ENABLE_STACKTRACES": True,
}
DEBUG_TOOLBAR_PATCH_SETTINGS = False
INSTALLED_APPS.append("debug_toolbar")
MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"] + MIDDLEWARE
INTERNAL_IPS = ["127.0.0.1"]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_ROOT, 'db.sqlite3'),
    }
}
