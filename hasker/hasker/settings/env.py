import os

import dj_database_url

from django.core.exceptions import ImproperlyConfigured

from .base import *  # noqa


msg = "Set the %s environment variable"


def get_env_variable(var_name, *args):
    try:
        return os.environ.get(var_name, *args)
    except KeyError:
        error_msg = msg % var_name
        raise ImproperlyConfigured(error_msg)


SECRET_KEY = get_env_variable("SECRET_KEY")
STATIC_ROOT = get_env_variable("STATIC_ROOT")
# STATICFILES_STORAGE = get_env_variable('STATICFILES_STORAGE')
MEDIA_ROOT = get_env_variable("MEDIA_ROOT")
ALLOWED_HOSTS = get_env_variable("ALLOWED_HOSTS", "").split(";")

# serve static and media files via Django
SERVE_STATIC = get_env_variable("DJANGO_SERVE_STATIC", False) == "True"
SERVE_MEDIA = get_env_variable("DJANGO_SERVE_MEDIA", False) == "True"


DATABASE_URL = get_env_variable("DATABASE_URL")
if not DATABASE_URL:
    raise ImproperlyConfigured("DATABASE_URL can't be None")

db_kwargs = {}
if DATABASE_URL.startswith("postgres"):
    db_kwargs = dict(conn_max_age=500, ssl_require=True)
db_from_env = dj_database_url.config(default=DATABASE_URL, **db_kwargs)
DATABASES = {"default": db_from_env}
