# Generic templates

Generic templates to display different common django objects.  Forms, lists,
items.  Probably should not be used in production but might be useful to
bootstrap new applications.
