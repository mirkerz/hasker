from django.db import models
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _
from django.contrib.contenttypes.fields import (
    GenericForeignKey,
    GenericRelation,
)
from django.contrib.contenttypes.models import ContentType

from common.models import TimestampMixin

User = get_user_model()


class Upvote(models.Model):
    """ Storage of upvotes made by user to some random object """

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="upvotes"
    )
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")

    @classmethod
    def create(cls, user, obj):
        """ Create upvote record if it's possible. Do nothing if not """
        if cls.upvote_exists(user, obj):
            return None
        upvote = cls(content_object=obj, user=user)
        obj.upvotes_count = obj.upvotes_count + 1
        obj.save(update_fields=["upvotes_count"])
        upvote.save()
        return 1

    @classmethod
    def upvote_exists(cls, user, obj):
        """ Shortcut for existing queryset """
        return obj.upvotes.filter(user=user.id).exists()


class AbstractUpvotesHost(models.Model):
    """ Abstract class to store upvotes counts and generic relation to upvote
    instances """

    upvotes_count = models.PositiveIntegerField(
        verbose_name=_("upvotes_count"), default=0, db_index=True
    )
    upvotes = GenericRelation(Upvote)

    class Meta:
        abstract = True


class Question(AbstractUpvotesHost, TimestampMixin, models.Model):
    title = models.CharField(_("title"), max_length=120)
    text = models.TextField(_("question"))
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="questions"
    )
    selected_answer = models.OneToOneField(
        "Answer",
        on_delete=models.SET_NULL,
        related_name="aswer_to",
        null=True,
        blank=True,
    )

    def user_can_select_answer(self, user):
        """ Is user can select correct answer """
        return user.id == self.user_id

    def user_can_answer(self, user):
        """ User can answer only once and can't be author  """
        if user.id == self.user_id:
            return False
        return not self.answers.filter(user=user).exists()

    def get_answers(self):
        """ Get answers as a qs sorted by upvotes with selected on top
        """
        queryset = self.answers
        # Expression can be initialized at some other place
        selected_expression = models.ExpressionWrapper(
            models.F("pk") - 1, output_field=models.BooleanField()
        )
        # Using selected_answer we cache queryset which might backfire at some
        # point. Might not.
        if self.selected_answer:
            queryset = queryset.annotate(
                is_selected=selected_expression
            ).order_by("is_selected", "-upvotes_count")
        else:
            queryset = queryset.order_by("-upvotes_count")
        return queryset

    def get_absolute_url(self):
        return reverse("qa:question_detail", kwargs={"pk": self.pk})

    class Meta:
        verbose_name = _("question")
        verbose_name_plural = _("questions")


class Answer(AbstractUpvotesHost, TimestampMixin, models.Model):
    """ Answers to questions: combination of user and questions should be
    unique together what can't be done by unique together
    """

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="answers"
    )
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, related_name="answers"
    )
    text = models.TextField(_("answer"))

    def get_absolute_url(self):
        return self.question.get_absolute_url()

    def mark_selected(self):
        Question.objects.filter(pk=self.question_id).update(
            selected_answer=self
        )

    @property
    def selected(self):
        return self.question.seleted_answer == self

    class Meta:
        verbose_name = _("answer")
        verbose_name_plural = _("answers")


class Tag(models.Model):
    """ Hight tec tags """

    title = models.CharField(_("title"), max_length=100, db_index=True)
