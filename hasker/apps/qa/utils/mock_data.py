"""
Utils to fill database with mock data
"""
import random
import string

from django.contrib.auth import get_user_model

from ..models import Question, Answer


__all__ = ["load_questions"]


User = get_user_model()
WORDS_COUNT = 50
USERNAMES = ["foo", "bar", "quu"]
LETTERS = string.ascii_lowercase


def load_questions(usernames=None):
    """ Generate users if needed and fill database with questions and answerse
    made by those users
    """
    if not usernames:
        usernames = USERNAMES
    generate_users(usernames)
    for user in User.objects.filter(username__in=usernames):
        generate_question(user)


def random_string(max_length=10):
    """ Generate random string with length up to max_length containing lowercase
    ascii symbols
    """
    length = random.randint(1, max_length)
    return "".join([random.choice(LETTERS) for i in range(length)])


def random_text(words):
    """ Generate random text with given number of words. Insert
    spaces, commas, stops and new lines at random places
    """
    words = [random_string() for i in range(words)]
    word = words[0]
    text = ["%s%s" % (word[0].upper(), word[-1])]
    for word in words[1:]:
        toss = random.random()
        if toss < 0.5:
            text.append(" %s" % word)
        elif toss < 0.6:
            text.append(", %s" % word)
        elif toss < 0.97:
            text.append(". %s%s" % (word[0].upper(), word[-1]))
        else:
            text.append(".\n%s%s" % (word[0].upper(), word[-1]))
    text.append(".")
    return "".join(text)


def generate_question(user):
    """ Save to database random questions made by user and attach answers
    made by other users
    """
    counter = sum(random.randint(0, 4) for i in range(3))
    for count in range(counter):
        question = Question.objects.create(
            title=random_text(5)[:100],
            text=random_text(100),
            upvotes_count=random.randint(0, 100),
            user=user,
        )

        users = User.objects.filter(username__in=USERNAMES).exclude(
            id=question.user_id
        )
        for user in users:
            if random.random() > 0.7:
                continue
            Answer.objects.create(
                text=random_text(100),
                upvotes_count=random.randint(0, 100),
                question=question,
                user=user,
            )


def generate_users(usernames):
    """ Create users with given usernames and username@example.com emails """
    for username in usernames:
        user, _ = User.objects.get_or_create(
            username=username, defaults={"email": "%s@example.com" % username}
        )
