from datetime import timedelta

from django.http import HttpResponseNotAllowed, JsonResponse
from django.shortcuts import get_object_or_404, Http404, redirect
from django.views.generic import DetailView, ListView
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.db.models import Count

from .forms import AnswerForm
from .models import Answer, Question, Upvote


class SelectedQuestions(ListView):
    """ Display limited list of most upvoted questions that a newer that sertain
    time limit
    """

    template_name = "qa/selected_questions_list.html"
    date_limit = timedelta(days=7)
    count_limit = 10

    def get_queryset(self):
        return (
            Question.objects.all()
            .filter(date_created__gte=timezone.now() - self.date_limit)
            .select_related("user")
            .annotate(answers_count=Count("answers"))
            .order_by("-upvotes_count")[: self.count_limit]
        )


class QuestionView(DetailView):
    model = Question

    def get_queryset(self):
        queryset = super().get_queryset()
        return (
            queryset.select_related("user")
            .prefetch_related("answers")
            .prefetch_related("answers__user")
        )

    def get_context_data(self, **kwargs):
        """ Display answer for and upvote url if needed """
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            show_answer_form = self.object.user_can_answer(self.request.user)
            context["show_answer_form"] = show_answer_form
            if show_answer_form:
                context["answer_form"] = AnswerForm()

            context["show_select_url"] = self.object.user_can_select_answer(
                user=self.request.user
            )
        return context


@login_required
def create_answer(request, question_id):
    if "POST" != request.method.upper():
        return HttpResponseNotAllowed(("post",))
    question = get_object_or_404(Question, pk=question_id)
    if not question.user_can_answer(request.user):
        raise PermissionDenied
    form = AnswerForm(request.POST)
    if form.is_valid():
        answer = form.save(commit=False)
        answer.question = question
        answer.user = request.user
        answer.save()
    return redirect(question.get_absolute_url())


@login_required
def select_answer(request, pk):
    answer_id = pk
    answer = (
        Answer.objects.filter(pk=answer_id).select_related("question").get()
    )
    if answer.question.user != request.user:
        raise Http404()
    answer.mark_selected()
    return redirect(answer.get_absolute_url())


def create_upvote_handler(cls):
    @login_required
    def create_upvote(request, pk):
        if not request.user.is_authenticated:
            if request.is_ajax():
                return JsonResponse(
                    {"result": "premission_denied"}, status=403
                )
            else:
                raise PermissionDenied
        obj = get_object_or_404(cls, pk=pk)
        upvote = Upvote.create(request.user, obj)
        if request.is_ajax():
            if upvote:
                status = 201
            else:
                status = 200
            return JsonResponse({"result": "created"}, status=status)
        return redirect(obj.get_absolute_url())

    return create_upvote


upvote_question = create_upvote_handler(Question)
upvote_answer = create_upvote_handler(Answer)
