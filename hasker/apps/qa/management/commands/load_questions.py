from django.core.management.base import BaseCommand

from qa.utils import load_questions


class Command(BaseCommand):

    def handle(self, *args, **options):
        load_questions()
