from django.test import Client, TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model

from .utils import load_questions
from .models import Answer, Question, Upvote

User = get_user_model()


def get_upvotes(obj):
    """ Get upvotes counter for question or answer stored in upvotes_count
    'cache' fetching it from database. refrest_from_db is for weak.
    """
    return (
        type(obj)
        .objects.filter(pk=obj.pk)
        .values("upvotes_count")
        .get()["upvotes_count"]
    )


class UserTestCase(TestCase):
    def test_load_questions(self):
        load_questions()
        self.assertTrue(Question.objects.all().count() > 3)
        self.assertTrue(User.objects.all().count() == 3)


class QATestCase(TestCase):
    def setUp(self):
        """ Create 3 users, one question and one answer """
        usernames = ["foo", "bar", "quu"]
        for username in usernames:
            user = User.objects.create(
                username=username, email=f"{username}@example.com"
            )
            setattr(self, f"user_{username}", user)
        self.question = Question.objects.create(
            user=self.user_foo, title="Question title", text="Question test"
        )
        self.answer = Answer.objects.create(
            user=self.user_bar, question=self.question, text="Answer text"
        )

    def test_question_url(self):
        """ Validate that question can generate proper url """
        url = Question.objects.all().first().get_absolute_url()
        response = Client().get(url)
        self.assertEqual(response.status_code, 200)

    def test_answer_url(self):
        """ Answer url is the same as question """
        self.assertEqual(
            self.question.get_absolute_url(), self.answer.get_absolute_url()
        )

    def test_question_user_can_answer(self):
        """ User can answer only once and can't be author  """
        self.assertFalse(self.question.user_can_answer(self.user_foo))
        self.assertFalse(self.question.user_can_answer(self.user_bar))
        self.assertTrue(self.question.user_can_answer(self.user_quu))

    def test_upvote_creation(self):
        """ Test model methods to create upvote """

        obj = self.question
        user = self.user_foo
        upvotes = get_upvotes(obj)
        self.assertFalse(Upvote.objects.filter(user=user).exists())
        self.assertEqual(Upvote.create(user, obj), 1)
        self.assertEqual(upvotes + 1, get_upvotes(obj))
        self.assertTrue(Upvote.objects.filter(user=user).exists())
        self.assertIsNone(Upvote.create(user, obj))
        self.assertEqual(upvotes + 1, get_upvotes(obj))

    def test_selected_questions_view(self):
        """ Check that our solo question is shown on main page """
        client = Client()
        response = client.get(reverse("qa:selected_questions_list"))
        self.assertEqual(response.status_code, 200)
        self.assertIn(self.question, response.context["object_list"])

    def test_question_view(self):
        client = Client()
        url = reverse("qa:question_detail", kwargs={'pk': self.question.pk})
        response = client.get(url)
        self.assertEqual(self.question, response.context["object"])
        self.assertNotIn('answer_form', response.context)

        client.force_login(self.user_foo)
        response = client.get(url)
        self.assertNotIn('answer_form', response.context)
        client.force_login(self.user_bar)
        response = client.get(url)
        self.assertNotIn('answer_form', response.context)
        client.force_login(self.user_quu)
        response = client.get(url)
        self.assertIn('answer_form', response.context)

    def test_answer_creation_view(self):
        """ Create answer via vew form """
        # Check that answer does not exists
        qs = Answer.objects.filter(
            user=self.user_quu,
            text="new_answer_text",
            question=self.question.id,
        )
        self.assertFalse(qs.exists())
        # Prepare http client
        client = Client()
        # Get url for answer creation
        url = reverse(
            "qa:question_answer", kwargs={"question_id": self.question.id}
        )
        # Make sure that non logged user is redirected
        response = client.post(url, {"text": "new_answer_text"})
        self.assertEqual(response.status_code, 302)
        # Login user
        client.force_login(self.user_quu)
        # Create request to create answer
        response = client.post(url, {"text": "new_answer_text"}, follow=True)
        # Validate that we get redirection chain to question page
        self.assertIsInstance(response.redirect_chain, list)
        self.assertListEqual(
            response.redirect_chain, [(self.question.get_absolute_url(), 302)]
        )
        # Validate that new answer created
        qs = Answer.objects.filter(
            user=self.user_quu,
            text="new_answer_text",
            question=self.question.id,
        )
        self.assertTrue(qs.exists())

    def test_upvote_view(self):
        """ test views of upvote_question and upvote_answer """
        object_list = [
            (Question.objects.all().first(), "qa:upvote_question"),
            (Answer.objects.all().first(), "qa:upvote_answer"),
        ]
        for obj, pattern in object_list:
            client = Client()

            response = client.get(reverse(pattern, kwargs={"pk": obj.pk}))
            self.assertEqual(response.status_code, 302)
            self.assertEqual(get_upvotes(obj), 0)

            client.force_login(self.user_foo)
            response = client.get(
                reverse(pattern, kwargs={"pk": obj.pk}), follow=True
            )
            self.assertListEqual(
                response.redirect_chain, [(obj.get_absolute_url(), 302)]
            )
            response = client.get(reverse(pattern, kwargs={"pk": 42}))
            self.assertEqual(response.status_code, 404)

            client.force_login(self.user_foo)
            response = client.get(
                reverse(pattern, kwargs={"pk": obj.pk}), follow=True
            )
            self.assertListEqual(
                response.redirect_chain, [(obj.get_absolute_url(), 302)]
            )
            self.assertEqual(get_upvotes(obj), 1)

    def test_answers_listing(self):
        second_answer = Answer.objects.create(
            user=self.user_quu, question=self.question, text="tmp answer text",
            upvotes_count=50,
        )
        self.assertListEqual(
            list(self.question.get_answers().values_list('id', flat=True)),
            [second_answer.id, self.answer.id],
            'Answers are not ordered by upvotes or something')
        self.answer.mark_selected()
        # reset queryset cache
        question = Question.objects.all().get(id=self.question.id)
        self.assertListEqual(
            list(question.get_answers().values_list('id', flat=True)),
            [self.answer.id, second_answer.id],
            'Selected answer should be first in list')

    def test_answer_selection(self):
        client = Client()
        url = reverse(
            "qa:select_answer", kwargs={"pk": self.question.id}
        )
        question_url = reverse("qa:question_detail", kwargs={'pk': self.question.pk})
        response = client.get(question_url)
        # Make sure that non logged user is redirected
        response = client.get(url)
        self.assertEqual(response.status_code, 302)

        client.force_login(self.user_quu)
        response = client.get(url)
        self.assertNotIn(
            'show_select_url',
            response.context,
            'Only question author should be able to select answer')
        self.assertEqual(response.status_code, 404)

        client.force_login(self.user_foo)
        response = client.get(url, follow=True)
        self.assertTrue(
            response.context['show_select_url'],
            'Question author should be able to select answer')
        self.assertListEqual(
            response.redirect_chain, [(self.question.get_absolute_url(), 302)]
        )

        url = reverse("qa:question_detail", kwargs={'pk': self.question.pk})
        response = client.get(url)
        self.assertEqual(self.question, response.context["object"])
        self.assertNotIn('answer_form', response.context)

        client.force_login(self.user_foo)
        response = client.get(url)
        self.assertNotIn('answer_form', response.context)
