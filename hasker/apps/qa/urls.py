from django.urls import path

from . import views

app_name = "qa"
urlpatterns = [
    path(
        "", views.SelectedQuestions.as_view(), name="selected_questions_list"
    ),
    path(
        "question/<int:pk>/",
        views.QuestionView.as_view(),
        name="question_detail",
    ),
    path(
        "question/<int:question_id>/answer/",
        views.create_answer,
        name="question_answer",
    ),
    path(
        "question/<int:pk>/upvote/",
        views.upvote_question,
        name="upvote_question",
    ),
    path("answer/<int:pk>/upvote/", views.upvote_answer, name="upvote_answer"),
    path("answer/<int:pk>/select/", views.select_answer, name="select_answer"),
]
