"""
Common abstract model mixins
"""

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _


class TimestampMixin(models.Model):
    date_created = models.DateTimeField(
        _("Date created"), default=timezone.now, db_index=True
    )

    class Meta:
        abstract = True
