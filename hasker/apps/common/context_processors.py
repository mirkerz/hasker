TREE = {
    '/account': 'account',
    '/profile': 'account',
    '/registration': 'account',
    '/': 'home',
}


def site_tree(request):
    for prefix, app in TREE.items():
        if request.path.startswith(prefix):
            return {'location': app}
    return {}
