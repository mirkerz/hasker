from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.staticfiles.templatetags.staticfiles import static


class User(AbstractUser):
    """ Override django User model just for the hell of it """

    # url to be used as default avatar
    default_avatar = static("avatar.jpg")
    avatar = models.ImageField(blank=True, null=True, upload_to='avatars')

    def get_avatar(self):
        """ Helper to extract avatar url. If it's not present returns
        image from static
        """
        if not self.avatar.name:
            return self.default_avatar
        return self.avatar.url
