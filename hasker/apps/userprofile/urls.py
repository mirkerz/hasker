from django.urls import path

from . import views

app_name = "profile"
urlpatterns = [
    path("profile/", views.ProfileUpdateView.as_view(), name="profile"),
    path("create/", views.ProfileCreateView.as_view(), name="profile_create"),
    path("complete/", views.ProfileCompleteView.as_view(), name="profile_created"),
]
