from django.test import TestCase

from .models import User


class UserTestCase(TestCase):
    def setUp(self):
        User.objects.create(username='test_user', email='foo@bar.com')

    def test_default_avatar(self):
        """ Make sure that for users witout avatar default is set """
        user = User.objects.get(username='test_user')
        self.assertEqual(user.get_avatar(), User.default_avatar)
