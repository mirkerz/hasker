from django.urls import reverse_lazy as reverse
from django.views.generic import CreateView, TemplateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import UserForm, UserRegisterForm


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    form_class = UserForm
    template_name = "userprofile/profile.html"
    success_url = reverse("profile:profile")

    def get_object(self, **kwargs):
        return self.request.user

    def get_contest_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Profile"
        return context


class ProfileCreateView(CreateView):
    form_class = UserRegisterForm
    template_name = "userprofile/register.html"
    success_url = reverse("profile:profile_created")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Register"
        return context


class ProfileCompleteView(TemplateView):
    template_name = "userprofile/register_complete.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Registration complete"
        return context
