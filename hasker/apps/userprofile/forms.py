from django import forms
from django.contrib.auth.forms import UserCreationForm

from .models import User


class UserForm(forms.ModelForm):
    class Meta:
        fields = ["avatar", "email", "first_name", "last_name"]
        model = User


class UserRegisterForm(UserCreationForm):
    class Meta:
        fields = ["username", "email"]
        model = User
