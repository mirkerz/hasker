# Dockerfile for building production container images

# Build python application using empty SQLITE and docker.build.env parameters
# to collect static and all that shit
# from python:3.7 as pythonbuilder
from python:3.7
env DEBIAN_FRONTEND=noninteractive
run apt-get update -qq && \
    apt-get install -q -y gettext && \
    rm -rf /var/lib/apt/lists/*
env PIP_DISABLE_PIP_VERSION_CHECK=1
run pip install -q pipenv
workdir /app
copy Pipfile* /app/
run PIPENV_NOSPIN=1 \
    pipenv --bare lock --requirements  > requirements.txt && \
    rm -rf /root/.local/share/virtualenvs && \
    echo 'uwsgi==2.0.*' >> requirements.txt

run pip -q wheel \
        --wheel-dir /wheel \
        --find-links /wheel \
        --no-cache-dir \
        -r requirements.txt
run pip install \
        --find-links /wheel \
        --no-index \
        --no-cache-dir \
        -r requirements.txt

label project=hasker
env DEBUG=False \
    STATICFILES_STORAGE=django.contrib.staticfiles.storage.ManifestStaticFilesStorage \
    DJANGO_SETTINGS_MODULE=hasker.settings.env \
    STATIC_ROOT=/app_static \
    MEDIA_ROOT=/app_media

# Bare in mind that running this command in developemend directory will also
# copy trash from .gitgnore that would never shows up in production builder
copy hasker /app
run mkdir -p /app_media && \
    DATABASE_URL=sqlite:////tmp/no.db \
    SECRET_KEY=BUILD \
    python manage.py collectstatic -v0 --noinput && \
    rm -rf /app_media

# run find /app_static -type f -size +200c ! -iname '*.gz' -execdir gzip -9 --keep --force {} \;
# run DATABASE_URL=sqlite:////tmp/no.db \
#     ENV_FILE=/app/docker.build.env \
#     python manage.py compilemessages --no-color
run python -m compileall -q /app/

workdir /app
# copy --from=pythonbuilder /wheel /wheel
# copy --from=pythonbuilder /app/requirements.txt /app/requirements.txt
# copy --from=pythonbuilder /app /app/
# copy --from=pythonbuilder /app_static /app_static/

CMD gunicorn hasker.wsgi:application --bind 0.0.0.0:$PORT
