# Hasker
## Poor Man's Stackoverflow
Crowd management QA system. Users can register, manage some basic information
like username, avatar, something else. Authenticated uses might ask and answer
questions, comment, vote and generally be excited.

## Requirements
+ Django 2.2

Details managed by `pipenv` and for some platforms it should by copied to
requirements: `pipenv lock -r > requirements.txt`
